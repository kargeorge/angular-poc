# ANIXE | Angular

#### Intro

A small angular 6 application which displays a list of car drivers consuming a REST api.
It demonstrates type safety with a service oriented design pattern maintaining small Components
and using them as proxy between the view and the services.

The only external library included is [Lodash](https://lodash.com/) a JavaScript utility library
with a variety of tools handling Objects and Arrays. However, the use of the specific library was
minimal and was only used in some cases that Angular 6 and RxJS could not handle.

Last but not least some small testing with Karma was made as a proof of concept. 

#### Build

>Verify that you have installed at least node 8.9.x (or greater) and npm 5.x.x by executing the commands node -v and npm -v in a terminal window.

In order to run the application 

1 Go to project folder and install dependencies.

`npm install
`

2 Launch development server.

`npm start
`

* Run tests with karma and jasmine.

`npm run test`


#### License
MIT
