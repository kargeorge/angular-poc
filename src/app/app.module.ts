import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from "@angular/common/http";
import { DriversListComponent } from './drivers-list/drivers-list.component';
import { DriversDetailComponent } from './drivers-detail/drivers-detail.component';
import { DriverTableComponent } from './components/driver-table/driver-table.component';
import { SortableHeaderComponent } from './components/sortable-header/sortable-header/sortable-header.component';
import {SearchFilterComponent} from "./components/search-filter/search-filter.component";

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    DriversListComponent,
    DriversDetailComponent,
    DriverTableComponent,
    SortableHeaderComponent,
    SearchFilterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,// import HttpClientModule after BrowserModule always.
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
