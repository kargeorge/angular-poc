export class ConstructorModel{
  private _constructorId: string;
  private _name: string;
  private _nationality: string;
  private _url: string;


  constructor(input?: any) {
    if(input){
      this._constructorId = input['constructorId'];
      this._name = input['name'];
      this._nationality = input['nationality'];
      this._url = input['url'];
    }
  }


  get constructorId(): string {
    return this._constructorId;
  }

  set constructorId(value: string) {
    this._constructorId = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get nationality(): string {
    return this._nationality;
  }

  set nationality(value: string) {
    this._nationality = value;
  }

  get url(): string {
    return this._url;
  }

  set url(value: string) {
    this._url = value;
  }
}
