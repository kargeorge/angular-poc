import {StandingModel} from "./standing-model";

export class SeasonModel {
  private _round :Number;
  private _season: string;
  private _listOfStandings: StandingModel[] = [];


  constructor(input?: any) {
    if(input){
      this._round = input['round'];
      this._season = input['season'];
      for(let standing of input['DriverStandings']){
        this._listOfStandings.push(new StandingModel(standing));
      }
    }
  }


  get round(): Number {
    return this._round;
  }

  set round(value: Number) {
    this._round = value;
  }

  get season(): string {
    return this._season;
  }

  set season(value: string) {
    this._season = value;
  }

  get listOfStandings(): StandingModel[] {
    return this._listOfStandings;
  }

  set listOfStandings(value: StandingModel[]) {
    this._listOfStandings = value;
  }
}
