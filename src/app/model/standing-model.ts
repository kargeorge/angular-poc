import {DriverModel} from "./driver-model";
import {ConstructorModel} from "./constructor-model";

export class StandingModel{
  private _points: Number;
  private _position: Number;
  private _positionText: Number;
  private _wins: Number;
  private _driver: DriverModel;
  private _listOfConstructor : ConstructorModel[] = [];


  constructor(input?:any) {
    if(input){
      this._points = input['points'];
      this._position = input['position'];
      this._positionText = input['positionText'];
      this._wins = Number(input['wins']);
      this._driver = new DriverModel(input['Driver']);
      for (let constructor of input['Constructors']){
        this._listOfConstructor.push(new ConstructorModel(constructor));
      }
    }
  }


  get points(): Number {
    return this._points;
  }

  set points(value: Number) {
    this._points = value;
  }

  get position(): Number {
    return this._position;
  }

  set position(value: Number) {
    this._position = value;
  }

  get positionText(): Number {
    return this._positionText;
  }

  set positionText(value: Number) {
    this._positionText = value;
  }

  get wins(): Number {
    return this._wins;
  }

  set wins(value: Number) {
    this._wins = value;
  }

  get driver(): DriverModel {
    return this._driver;
  }

  set driver(value: DriverModel) {
    this._driver = value;
  }

  get listOfConstructor(): ConstructorModel[] {
    return this._listOfConstructor;
  }

  set listOfConstructor(value: ConstructorModel[]) {
    this._listOfConstructor = value;
  }

  get firstConstructorName(): string{
    return this._listOfConstructor[0].name;
  }

  get driverFullName():string{
    return this._driver.getFullName();
  }
}
