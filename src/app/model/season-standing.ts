import {SeasonModel} from "./season-model";

export class SeasonStanding {

  private _listOfSeasons : SeasonModel[] = [];
  private _season: string;

  constructor(input?: any) {
    this._season  = input['season'];
    for (let season of input['StandingsLists']) {
      this._listOfSeasons.push(new SeasonModel(season));
    }
  }


  get listOfSeasons(): SeasonModel[] {
    return this._listOfSeasons;
  }

  set listOfSeasons(value: SeasonModel[]) {
    this._listOfSeasons = value;
  }

  get season(): string {
    return this._season;
  }

  set season(value: string) {
    this._season = value;
  }
}
