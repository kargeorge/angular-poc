export class DriverModel {

  private _code: string;
  private _dateOfBirth: string;
  private _driverId: string;
  private _familyName: string;
  private _givenName: string;
  private _nationality: string;
  private _permanentNumber: string;
  private _url: string;


  constructor(input?:any) {
    if(input){
      this._code = input['code'];
      this._dateOfBirth = input['dateOfBirth'];
      this._driverId = input['driverId'];
      this._familyName = input['familyName'];
      this._givenName = input['givenName'];
      this._nationality = input['nationality'];
      this._permanentNumber = input['permanentNumber'];
      this._url = input['url'];
    }
  }


  get code(): string {
    return this._code;
  }

  set code(value: string) {
    this._code = value;
  }

  get dateOfBirth(): string {
    return this._dateOfBirth;
  }

  set dateOfBirth(value: string) {
    this._dateOfBirth = value;
  }

  get driverId(): string {
    return this._driverId;
  }

  set driverId(value: string) {
    this._driverId = value;
  }

  get familyName(): string {
    return this._familyName;
  }

  set familyName(value: string) {
    this._familyName = value;
  }

  get givenName(): string {
    return this._givenName;
  }

  set givenName(value: string) {
    this._givenName = value;
  }

  get nationality(): string {
    return this._nationality;
  }

  set nationality(value: string) {
    this._nationality = value;
  }

  get permanentNumber(): string {
    return this._permanentNumber;
  }

  set permanentNumber(value: string) {
    this._permanentNumber = value;
  }

  get url(): string {
    return this._url;
  }

  set url(value: string) {
    this._url = value;
  }

  getFullName(){
    return this._familyName + ' ' + this._givenName;
  }
}
