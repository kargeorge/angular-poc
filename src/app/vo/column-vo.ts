export class ColumnVo {
  private _name: string;
  private _value: string;
  private _direction:boolean;

  constructor(name: string, value: string) {
    this._name = name;
    this._value = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get value(): string {
    return this._value;
  }

  set value(value: string) {
    this._value = value;
  }


  get direction(): boolean {
    return this._direction;
  }

  set direction(value: boolean) {
    this._direction = value;
  }
}
