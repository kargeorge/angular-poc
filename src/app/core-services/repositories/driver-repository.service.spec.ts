import { TestBed } from '@angular/core/testing';

import { DriverRepositoryService } from './driver-repository.service';
import {HttpClientModule} from "@angular/common/http";

describe('DriverRepositoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[HttpClientModule]
  }));

  it('should be created', () => {
    const service: DriverRepositoryService = TestBed.get(DriverRepositoryService);
    expect(service).toBeTruthy();
  });
});
