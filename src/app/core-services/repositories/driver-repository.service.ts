/**
 * Repository for the season standings raw data response from the server.
 * Each module will have a separate repository service in order to have a service oriented pattern and the retrieving
 * of data to be decoupled from the actual manipulation of data.
 */
import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/index";
import {map} from "rxjs/internal/operators";
import {SeasonStanding} from "../../model/season-standing";

@Injectable({
  providedIn: 'root'
})
export class DriverRepositoryService {

  constructor(private http: HttpClient) { }

  /**
   * Retrieves a Season Standing from the remote source.
   * @returns {Observable<SeasonStandings>}
   */
  getDriverList(): Observable<SeasonStanding> {
    return this.http.get<SeasonStanding>('http://ergast.com/api/f1/2013/driverStandings.json').pipe(map(response => {
      return new SeasonStanding(response['MRData']['StandingsTable']);
    }))
  }
}
