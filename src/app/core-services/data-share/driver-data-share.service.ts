/**
 * Statefull service that shares data between components.
 */
import { Injectable } from '@angular/core';
import {StandingModel} from "../../model/standing-model";

@Injectable({
  providedIn: 'root'
})
export class DriverDataShareService {

  constructor() { }

  listOfStandings: StandingModel[] = [];

}
