import { TestBed } from '@angular/core/testing';

import { DriverDataShareService } from './driver-data-share.service';

describe('DriverDataShareService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DriverDataShareService = TestBed.get(DriverDataShareService);
    expect(service).toBeTruthy();
  });
});
