import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {DriversListComponent} from "./drivers-list/drivers-list.component";
import {DriversDetailComponent} from "./drivers-detail/drivers-detail.component";

const routes: Routes = [
  {
    path: 'drivers-list',
    component: DriversListComponent,
    children: [
      {
        path: ':id',
        component: DriversDetailComponent
      }
    ]
  },
  { path: '',
    redirectTo: '/drivers-list',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {
}
