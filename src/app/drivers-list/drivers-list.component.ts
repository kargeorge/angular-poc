import {Component, OnInit} from '@angular/core';
import {DriverDetailService} from "../drivers-detail/driver-detail.service";
import {SeasonModel} from "../model/season-model";
import {DriverDataShareService} from "../core-services/data-share/driver-data-share.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-drivers-list',
  templateUrl: './drivers-list.component.html',
  styleUrls: ['./drivers-list.component.scss']
})
export class DriversListComponent implements OnInit {
  requestedSeason: String;
  currentSeason: SeasonModel = new SeasonModel();
  dataLoaded: boolean = false;

  constructor(private driverDetailService: DriverDetailService, private driverDataShareService: DriverDataShareService, private router: Router) {
    this.requestedSeason = '2013';
  }


  ngOnInit() {
    this.driverDetailService.getDriverRankingListBySeason(this.requestedSeason).subscribe((currentSeason: SeasonModel) => {
      this.currentSeason = currentSeason;

      //Put some data in the according sharing service in order to have access in other components or routes
      this.driverDataShareService.listOfStandings = this.currentSeason.listOfStandings;
      this.dataLoaded = true;
    });
  }

  checkCurrentRoute(): boolean {
    return this.router.url === '/drivers-list';
  }

}
