import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DriversListComponent} from './drivers-list.component';
import {RouterTestingModule} from "@angular/router/testing";
import {SearchFilterComponent} from "../components/search-filter/search-filter.component";
import {DriverTableComponent} from "../components/driver-table/driver-table.component";
import {SortableHeaderComponent} from "../components/sortable-header/sortable-header/sortable-header.component";
import {HttpClientModule} from "@angular/common/http";
import {SeasonModel} from "../model/season-model";

describe('DriversListComponent', () => {
  let component: DriversListComponent;
  let fixture: ComponentFixture<DriversListComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DriversListComponent, SearchFilterComponent, DriverTableComponent, SortableHeaderComponent],
      imports: [RouterTestingModule, HttpClientModule],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriversListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('To have list of standings', () => {
    component.driverDetailService.getDriverRankingListBySeason("2013").subscribe((currentSeason: SeasonModel) => {
      expect(currentSeason.listOfStandings.length).toBeGreaterThan(0)
    });
  });

  it('To not have list of standings', () => {
    component.driverDetailService.getDriverRankingListBySeason("2014").subscribe((currentSeason: SeasonModel) => {
      expect(currentSeason.listOfStandings.length).toEqual(0)
    });
  });

});
