import {Component, Input, OnInit} from '@angular/core';
import {DriverDetailService} from "./driver-detail.service";
import {DriverModel} from "../model/driver-model";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-drivers-detail',
  templateUrl: './drivers-detail.component.html',
  styleUrls: ['./drivers-detail.component.scss']
})
export class DriversDetailComponent implements OnInit {

  selectedDriver:DriverModel;

  constructor(private driverDetailService: DriverDetailService, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.selectedDriver = this.driverDetailService.getDriverByCode(params['id']);
    });
  }

  ngOnInit() {


  }

}
