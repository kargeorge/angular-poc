import { TestBed } from '@angular/core/testing';

import { DriverDetailService } from './driver-detail.service';
import {HttpClientModule} from "@angular/common/http";
import {DriverModel} from "../model/driver-model";
import Expected = jasmine.Expected;

describe('DriverDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[HttpClientModule]
  }));

  it('should be created', () => {
    const service: DriverDetailService = TestBed.get(DriverDetailService);
    expect(service).toBeTruthy();
  });
});
