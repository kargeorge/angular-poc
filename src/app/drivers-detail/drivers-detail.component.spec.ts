import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DriversDetailComponent} from './drivers-detail.component';
import {HttpClientModule} from "@angular/common/http";
import {ActivatedRoute, Params, Router} from "@angular/router";


describe('DriversDetailComponent', () => {
  let component: DriversDetailComponent;
  let fixture: ComponentFixture<DriversDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DriversDetailComponent],
      imports: [HttpClientModule],
      providers: [
        {provide: ActivatedRoute}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriversDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
