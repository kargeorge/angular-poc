/**
 * Service dedicated to transform and manipulate all the data that the component needs.
 * Ideally the component should not have any logic but to be a proxy between the view and the services that retrieving and transform the
 * remote data.
 */
import {Injectable} from '@angular/core';
import {DriverRepositoryService} from "../core-services/repositories/driver-repository.service";
import {Observable} from "rxjs/index";
import {SeasonModel} from "../model/season-model";
import {SeasonStanding} from "../model/season-standing";
import {map} from "rxjs/internal/operators";
import {DriverModel} from "../model/driver-model";
import {DriverDataShareService} from "../core-services/data-share/driver-data-share.service";
import {StandingModel} from "../model/standing-model";

@Injectable({
  providedIn: 'root'
})
export class DriverDetailService {

  constructor(private driverRepository: DriverRepositoryService, private driverDataShareService: DriverDataShareService) {
  }

  /**
   * Takes as argument a season and retrieves a Season Model which contains the Season Standings.
   * @param {String} season
   * @returns {Observable<SeasonModel>}
   */
  getDriverRankingListBySeason(season: String): Observable<SeasonModel> {
    return this.driverRepository.getDriverList().pipe(
      map((seasonStanding: SeasonStanding) => seasonStanding.listOfSeasons.find((seasonModel: SeasonModel) => seasonModel.season === season)),
    )
  }

  getDriverByCode(code: string): DriverModel {
    return this.driverDataShareService.listOfStandings.find(DriverDetailService.findDriverByCodePredicate(code)).driver;
  }

  /**
   * Function that returns an anonymous function as predicate to select a Standing model by driver code
   * @param {string} code
   * @returns {(standing: StandingModel) => boolean}
   */
  private static findDriverByCodePredicate(code:string) {
    return (standing: StandingModel): boolean => {
      return standing.driver.driverId === code;
    }
  }

}
