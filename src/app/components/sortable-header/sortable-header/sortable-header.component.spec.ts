import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortableHeaderComponent } from './sortable-header.component';
import {ColumnVo} from "../../../vo/column-vo";

describe('SortableHeaderComponent', () => {
  let component: SortableHeaderComponent;
  let fixture: ComponentFixture<SortableHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortableHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortableHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reset all column direction except the imported', () =>{
    component.headerColumns = [new ColumnVo("first", "1"), new ColumnVo("second", "2")];
    component.headerColumns[0].direction = true;
    component.resetColumns(component.headerColumns[0])
    expect(component.headerColumns[1].direction).toBeFalsy();
    expect(component.headerColumns[0].direction).toBeTruthy();
  })
});
