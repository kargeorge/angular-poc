import {Component, Input, OnInit} from '@angular/core';
import {StandingModel} from "../../../model/standing-model";
import {ColumnVo} from "../../../vo/column-vo";
import * as _ from 'lodash';

@Component({
  selector: 'app-sortable-header',
  templateUrl: './sortable-header.component.html',
  styleUrls: ['./sortable-header.component.scss']
})

export class SortableHeaderComponent implements OnInit {

  @Input()
  headerColumns: ColumnVo[] = [];

  @Input('rows')
  dataRows: StandingModel[];

  private originalDataRows: StandingModel[] = [];

  constructor() {

  }

  ngOnInit() {

  }

  sortBy(column: ColumnVo) {
    this.resetColumns(column);
    column.direction = !column.direction;
    console.log(column);
    /*
     * TODO: check if this sorting has any UX meaning because the index will not change just the driver assigned to it. This is because the spec describes the first column must be the index of the array
    * */
    if (column.name == "index") {
      this.dataRows.reverse();
    } else {
      this.dataRows.sort(SortableHeaderComponent.sortGeneral(column.direction, column.name))
    }
  }


  /**
   * Resets columns direction in order one sorting to exist each time.
   * @param {ColumnVo} currentColumn
   */
  private resetColumns(currentColumn: ColumnVo) :void{
    for (let column of this.headerColumns){
      if(column!==currentColumn){
        column.direction = undefined;
      }
    }
  }


  private resetSorting() {
    //keep the original data tha has been imported in order to reset the sorting
    if (this.originalDataRows.length == 0) {
      this.originalDataRows = _.cloneDeep(this.dataRows);
    }
    for (let i in this.dataRows) {
      this.dataRows[i] = this.originalDataRows[i];
    }
  }

  /**
   * Static functions that returns an anonymous functions as predicate to the sort of array.
   * @param {boolean} direction
   * @param {string} attribute
   * @returns {(a: StandingModel, b: StandingModel) => number}
   */
  private static sortGeneral(direction:boolean, attribute: string){
    return (a: StandingModel, b: StandingModel): number => {
      if(direction){
        return a[attribute] > b[attribute] ? 1 : a[attribute] === b[attribute] ? 0 : -1;
      }else{
        return a[attribute] < b[attribute] ? 1 : a[attribute] === b[attribute] ? 0 : -1;
      }
    }
  }

}
