import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverTableComponent } from './driver-table.component';
import {SortableHeaderComponent} from "../sortable-header/sortable-header/sortable-header.component";
import {RouterTestingModule} from "@angular/router/testing";
import {StandingModel} from "../../model/standing-model";

describe('DriverTableComponent', () => {
  let component: DriverTableComponent;
  let fixture: ComponentFixture<DriverTableComponent>;
  let listOfStandings = [new StandingModel(), new StandingModel()];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverTableComponent, SortableHeaderComponent ],
      imports:[RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('make array empty but not mutate', () => {
    listOfStandings.splice(0, listOfStandings.length);
    expect(listOfStandings.length).toBe(0);
  });

});
