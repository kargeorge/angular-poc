import {Component, Input} from '@angular/core';
import {StandingModel} from "../../model/standing-model";
import {ColumnVo} from "../../vo/column-vo";

@Component({
  selector: 'app-driver-table',
  templateUrl: './driver-table.component.html',
  styleUrls: ['./driver-table.component.scss']
})
export class DriverTableComponent {
  @Input() listOfStandings: StandingModel[] = [];

  columns:ColumnVo[] = [new ColumnVo('index', '#'),new ColumnVo('driverFullName', 'Driver’s Name'), new ColumnVo('firstConstructorName', 'Constructor’s Name'), new ColumnVo('wins', 'Wins')];

}
