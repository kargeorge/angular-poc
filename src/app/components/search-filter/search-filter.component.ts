import {Component, Input, OnInit} from '@angular/core';
import {StandingModel} from "../../model/standing-model";
import * as _ from "lodash";
import {DriverDataShareService} from "../../core-services/data-share/driver-data-share.service";

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.scss']
})
export class SearchFilterComponent implements OnInit {

  @Input() listOfStandings: StandingModel[];

  private allStandings: StandingModel[];

  constructor(private driverDataShareService: DriverDataShareService) {

  }

  ngOnInit() {
    //We clone the sharing data in order to have a new instance of the array and keep in memory the whole list of standings.
    this.allStandings = _.cloneDeep(this.driverDataShareService.listOfStandings);
  }

  /**
   * On key up a fire the below function is triggered and filters through the array of standings to locate a possible match.
   * @param event
   */
  onKeyUp(event: any) {
    let searchValue = _.lowerCase(event.target.value);
    let filteredArray = this.allStandings.filter((standing: StandingModel) => {
      return _.lowerCase(standing.driverFullName).includes(searchValue) || _.lowerCase(standing.firstConstructorName).includes(searchValue) || standing.wins === Number(searchValue);
    });
    this.listOfStandings.splice(0, this.listOfStandings.length);
    for (let i in filteredArray) {
      this.listOfStandings.push(filteredArray[i]);
    }
  }

}
